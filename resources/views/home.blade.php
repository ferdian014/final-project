@extends('layouts.app2')


@push('script')
<script src="{{asset('/js/swal.min.js')}}"></script>
<script>
    Swal.fire({
        title: "Log In Successful",
        text: "helps you connect and share with the people in your life ",
        icon: "success",
        confirmButtonText: "OK",
    });
</script>
@endpush

@section('content')


                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <h3>Selamat Datang  {{ Auth::user()->name }}</h3>
                    
   
</div>
@endsection
