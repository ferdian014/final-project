@extends('layouts.halaman2')

@section('content')
    <div class="card card-primary">
            <div class="card-header">
                    <h6 class="card-title">Pencarian Hasil Berdasarkan Nama : {{Request::get('pencarian')}}</h6>
            </div>
    </div>
    
    @if (count($searchusers)>0)
    @foreach ($searchusers as $user)

    <div class="row ">
        <div class="w3-col m3" >
            <div class="w3-card w3-round w3-white">  
                <div class="w3-container">
                     <h4 class="w3-center">{{$user->name}}</h4>
                     <p class="w3-center"><img src="{{asset('/images/ferdian.jpg')}}" class="w3-circle" style="height:106px;width:106px" alt="Avatar"></p>
                     <hr>
                     <h4 class="w3-center">{{$user->name}}</h4>
                </div>
                    <div class=" w3-blue w3-center">
                    <a href="{{route('beranda',$user->name)}}" style="text-decoration:none">
                    <h3>show profil</h3></a>
                     </div>
            </div>
        </div>
    </div> 
    @endforeach
       
    @else
    <h6>Nama yang anda cari tidak ditemukan</h6>
    @endif

    
@endsection
 