{{-- @yield('side1') --}}

<aside class="main-sidebar sidebar-dark-primary bg-light elevation-4">
    <!-- Brand Logo -->
    <a href="../../index3.html" class="brand-link">
      <img src="{{asset('/adminlte/dist/img/AdminLTELogo.png')}}"
           alt="AdminLTE Logo"
           class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">AdminLTE 3</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar text-white bg-info">
      <!-- Sidebar user (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        
        @guest
        <li class="nav-item">
            <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
        </li>
        @if (Route::has('register'))
            <li class="nav-item">
                <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
            </li>
        @endif
      @else
        <div class="info">
          <a href="#" class="d-block">{{ Auth::user()->name }}</a>
        </div>
        @endguest
      </div>
      


      <!-- Sidebar Menu -->
      
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
       @guest
               <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="fas fa-bars"></i>
              <p>
               Menu
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
                <li class="nav-item">
                  <a href="/" class="nav-link">
                    <i class="fas fa-home"></i>
                    <p>Halaman Utama</p>
                  </a>
                </li>
            
            </ul>
          </li>
         
          
              
          
         
        
     
          @else
            
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="fas fa-bars"></i>
              <p>
                  Menu
                  <i class="right fas fa-angle-left"></i>
                </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{route('profile',Auth::user()->name )}}" class="nav-link">
                  <i class="fas fa-user"></i>
                  <p>Profil</p>
                </a>
              </li>
                <li class="nav-item">
                  <a href="/" class="nav-link">
                    <i class="fas fa-home"></i>
                    <p>Halaman Utama</p>
                  </a>
                </li>
                <li class="nav-item">
                    <a href="" class="nav-link">
                      <i class="fas fa-home"></i>
                      <p>Halaman Postingan</p>
                    </a>
                  </li>
            </ul>
          </li>
        
      
          <li class="nav-link">
          <a class="fas fa-sign-out-alt" href="{{ route('logout') }}"
          onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();">
           {{ __('Logout') }}
       </a>

       <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
           @csrf
       </form>
          </li>
        
        </ul>
        @endguest
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>
    
