@extends('layouts.halaman2')

@section('content')

<div>
    <h2>Tambah Data</h2>
        <form action="/profile" enctype="multipart/form-data" method="POST">
            @csrf
            
            <div class="form-group">
                <label for="alamat">Alamat</label>
                <input type="text" class="form-control" name="alamat" placeholder="Masukkan alamat">
                @error('alamat')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            
            <div class="form-group">
                <label>Hobi</label>
                <input type="text" class="form-control" name="hobi" placeholder="Masukkan Hobi Anda">
                @error('hobi')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror

                <div class="form-group">
                    <label>telepone</label>
                    <input type="number" class="form-control" name="telepone" placeholder="Masukkan telepone">
                    @error('telepone')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror

            

          

                
                    
                    <form>
                        <div class="form-group">
                          <label>poster</label>
                          <input type="file" class="form-control-file" name="poster">
                        </div>
                      </form>
                      @error('poster')
                      <div class="alert alert-danger">
                           {{ $message }}
                      </div>
                  @enderror


            </div>
            <button type="submit" class="btn btn-primary">Tambah</button>
        </form>
</div>

@endsection
