<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();
//tidak butuh 
//Route::get('/home', 'HomeController@index')->name('home');
//Route::get('/profile', 'ProfileController@Profile');
//Route::get('/user/{user}','ProfilController@profil')->name('profil');
//Route::post('/search','SearchController@search')->name('search');
//tidak butuh
Route::get('/users','FollowingController@allUsers');

Route::get('/teman','FollowingController@teman')->name('teman');





Route::post('/pencarian','SearchController@pencarian')->name('pencarian');





Route::get('/beranda','ProfilController@beranda')->name('beranda');

route::get('/profile/create','ProfilController@create')->name('create');
route::post('/profile','ProfilController@store');
route::get('/profile','ProfilController@index');




Route::get('/following/{id}','FollowingController@following')->name('following');

Route::get('/following','FollowingController@followingUser')->name('followingUser');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
