<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;

class ProfileController extends Controller
{
//     public function index(User $user)
//     {
//       //dd($user);
//       $user = User::find($user);
      
//       return view('profile', compact('profile'));
//     }
// }
public function index()
{
    $profil = Profil::where('user_id', Auth::user()->id)->first();
    return view('profile.index', compact('profile'));
}

public function store(Request $request)
    {
        $this->validate($request,[
           
            'alamat' => 'required',
            'hobi' => 'required',
            'telepone'=> 'required',
            
        ]);

        
        Profil::create([
            "alamat" => $request->alamat,
            "hobi" => $request->hobi,
            "telepone" => $request->telepone,
            'user_id'=> Auth::user()->id
            ]);
    
            return redirect('/profile');
    
    }
