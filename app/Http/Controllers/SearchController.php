<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class SearchController extends Controller
{
    public function search(Request $request)
    {
        $searchInput = $request->input('search');;
        $searchusers = User::where('name','Like','%'.$searchInput. '%')->get();
        return view ('search.result', compact('searchusers'));
    }

    
    public function pencarian(Request $request)
    {
        $searchInput = $request->input('pencarian');
        $searchusers = User::where('name','Like','%'.$searchInput. '%')->get();
         return view ('search.pencarian', compact('searchusers'));
    }







}
