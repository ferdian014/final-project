<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\akun;
use DB;



use File;


class ProfilController extends Controller
{
    public function profile(User $user)
    {
      //dd($user);
      $user = User::find($user);
      
      return view('user.profile', compact('user'));
    }


    public function beranda(User $user)
    {
      //dd($user);
      $user = User::find($user);
      
      return view('user.beranda', compact('user'));
    }

    public function create()
    {
        // return view ('user.create');
    }



    public function store(request $request)
            {
            $request->validate([
              'alamat' => 'required|unique:akun',
              'hobi' => 'required',
              'telepone' => 'required',
              'poster' => 'mimes:jpeg,jpg,png|max:2200'
            ]);

            $query = DB::table('akun')->insert([
                'alamat' => $request["alamat"],
                'hobi' => $request["hobi"],
                'telepone' => $request["telepone"],
                'poster' => $request["poster"],
            ]);
            return redirect('/profile');
            }

 






public function index()
{
$cast = DB::table('akun')->get();
return view('user.index', compact('user')); 
}


}




