<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Follower;
use Auth;

class FollowingController extends Controller
{
    public function allUsers(){
        $allUsers = User::where('id', '!=', Auth::user()->id)->get();
        return view('user.allUsers', compact('allUsers'));
    } 


    public function teman(){
        $teman = User::where('id', '!=', Auth::user()->id)->get();
        return view('user.teman', compact('teman'));
    } 







 
    public function following($id)
    {
        $follow = New Follower;
        $follow->user_id = Auth::user()->id;
        $follow->follow_id = $id;
        $follow->save();
        
        return back();

    }

    public function followingUser()
    {
        $followingUser = Follower::where('user_id','=',Auth::user()->id)
        ->join('users','users.id','=','follow_id')
        ->where('status',2)
        ->get();

        return view('user.followingUser', compact('followingUser'));
    }


}
